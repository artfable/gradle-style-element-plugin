# Gradle Import Check Plugin

## Overview
Create web component style **files** from css
 

## Install
in _settings.gradle.kts_:
```kotlin
pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
    }
}
```

in build script:
```kotlin
plugins {
    id("artfable.style-element") version "0.0.2"
}
```

It'll add a task `styleElement`

## Usage

```kotlin
styleElement {
    group {
        // deleteCss = false // default
        source = "$projectDir/src/test/java"
        output = "$buildSrc/dist"
    }
    group {
        // ...
    }
}
```

Put `deleteCss = true` to delete source files after web-component file is created
