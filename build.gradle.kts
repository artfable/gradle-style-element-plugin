plugins {
    `kotlin-dsl`
    id("artfable.artifact") version "0.0.4"
    `maven-publish`
}

group = "com.artfable.gradle"
version = "0.0.2"

val gitlabToken = findProperty("gitlabPersonalApiToken") as String?

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(gradleApi())
    implementation(localGroovy())
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileJava {
        targetCompatibility = "1.8"
    }
    compileTestJava {
        targetCompatibility = "1.8"
    }
}

gradlePlugin {
    plugins {
        create("gradle-style-element-plugin") {
            id = "artfable.style-element"
            implementationClass = "com.artfable.gradle.style.element.StyleElementPlugin"
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(tasks.sourceJar)
            artifact(tasks.javadocJar)

            pom {
                description.set("Create web component style files from css")
                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://gitlab.com/artfable/gradle-style-element-plugin/master/LICENSE")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("artfable")
                        name.set("Artem Veselov")
                        email.set("art-fable@mail.ru")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/18086419/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}
