package com.artfable.gradle.style.element

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.io.File
import java.io.FileFilter
import java.util.*

/**
 * @author artfable
 * 12/04/2020
 */
class StyleElementPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        val config = project.extensions.create("styleElement", StyleElementExtension::class.java)

        project.tasks.create("styleElement") {
            doFirst {
                config.groups.forEach { group ->
                    if (group.source == null || group.output == null) {
                        throw IllegalArgumentException("Source & output should be provided")
                    }

                    val source = File(group.source!!)
                    val output = File(group.output!!)

                    if (!source.exists() || !source.isDirectory) {
                        throw IllegalArgumentException("Source [${group.source}] isn't valid")
                    }

                    if (!output.exists()) {
                        output.mkdir();
                    }

                    findCss(source).forEach { cssFile ->
                        logger.debug("processing ${cssFile.name}")
                        val path = output.absolutePath + File.separator + cssFile.parent.removePrefix(source.absolutePath)
                        File(path).mkdirs()

                        val outputFile = File(path + File.separator + cssFile.nameWithoutExtension + ".js")
                        if (outputFile.exists()) {
                            outputFile.delete()
                            outputFile.createNewFile()
                        }

                        outputFile.appendText("const styleElement = document.createElement('dom-module');\n" +
                                "styleElement.innerHTML = \n" +
                                "  `<template>\n" +
                                "    <style>")
                        outputFile.appendText(cssFile.readText())
                        outputFile.appendText("</style>\n" +
                                "  </template>`;\n" +
                                "styleElement.register('${cssFile.nameWithoutExtension}');")

                        if (group.deleteCss) {
                            cssFile.delete()
                        }
                    }

                }
            }
        }
    }

    private fun findCss(dir: File): Collection<File> {
        val files: MutableList<File> = LinkedList()

        dir.listFiles(FileFilter { it.isFile && it.extension == "css" })?.let { files.addAll(it) }
        dir.listFiles(FileFilter { it.isDirectory })?.forEach { files.addAll(findCss(it)) }

        return files
    }

}

open class StyleElementExtension {
    val groups: MutableList<StyleElementGroup> = LinkedList()

    fun group(action: Action<StyleElementGroup>) {
        groups.add(StyleElementGroup().apply(action::execute))
    }
}

open class StyleElementGroup {
    var deleteCss = false
    var source: String? = null
    var output: String? = null
}